let collection = [];

// Write the queue functions below.
function queue() {
    // this.items = {}; 
    // this.headIndex = 0; 
    // this.tailIndex = 0;
    this.head = null;
    this.tail = null;
    this.length = 0;
    
}

//this is correct
function print() {
    //add code here
    return collection;
}

function enqueue(element) {
    //add code here
    const newNode = {
        value: element,
        next: null,
      };
    
      if (this.isEmpty()) {
        this.head = newNode;
        this.tail = newNode;
      } else {
        this.tail.next = newNode;
        this.tail = newNode;
      }
    
      this.length++;
    
}

function dequeue() {
    //add code here
    if (this.isEmpty()) {
        return null;
      }
    
      const removedValue = this.head.value;
      this.head = this.head.next;
    
      if (this.isEmpty()) {
        this.tail = null;
      }
    
      this.length--;
    
      return removedValue;
}

function front() {
    //add code here
    return this.isEmpty() ? null : this.head.value;
}

function size() {
    //add code here
    return this.length;
}

//this is correct
function isEmpty() {
    //add code here
    if(this.tail - this.head == 0){
        return true;
    }
    return false;
}

module.exports = {
    collection,
    queue,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};